# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/luciano/tp2-metodos-numericos/src/eigen.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/metnum.dir/src/eigen.cpp.o"
  "/home/luciano/tp2-metodos-numericos/src/knn.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/metnum.dir/src/knn.cpp.o"
  "/home/luciano/tp2-metodos-numericos/src/metnum.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/metnum.dir/src/metnum.cpp.o"
  "/home/luciano/tp2-metodos-numericos/src/pca.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/metnum.dir/src/pca.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "metnum_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../eigen"
  "../pybind11/include"
  "/usr/include/python3.8"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
