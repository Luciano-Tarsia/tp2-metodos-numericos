# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/luciano/tp2-metodos-numericos/src/eigen.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/tp2.dir/src/eigen.cpp.o"
  "/home/luciano/tp2-metodos-numericos/src/knn.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/tp2.dir/src/knn.cpp.o"
  "/home/luciano/tp2-metodos-numericos/src/main.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/tp2.dir/src/main.cpp.o"
  "/home/luciano/tp2-metodos-numericos/src/pca.cpp" "/home/luciano/tp2-metodos-numericos/build/CMakeFiles/tp2.dir/src/pca.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../eigen"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
