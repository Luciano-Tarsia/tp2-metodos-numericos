#include <iostream>
#include "pca.h"
#include "eigen.h"

using namespace std;


PCA::PCA(unsigned int n_components) {
  	n = n_components;
}

void PCA::fit(Matrix X) {

  	Vector u = X.colwise().mean();
  	Matrix X_aux = (X.rowwise() - u.transpose()) / sqrt(X.rows() - 1);

  	Matrix Mx = X_aux.transpose() * X_aux;

  	pair<Vector, Matrix> eigens = get_first_eigenvalues(Mx, n, 1000, 10e-3);

  	transformation = eigens.second;
}


MatrixXd PCA::transform(Matrix X) {
  	return X * transformation;
}
