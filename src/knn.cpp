#include <algorithm>
#include <chrono>
#include <iostream>
#include <queue>
#include <vector>
#include "knn.h"
#include <utility>

using namespace std;

KNNClassifier::KNNClassifier(unsigned int n_neighbors)
{
	k = n_neighbors;	
}

void KNNClassifier::fit(Matrix X, Matrix y)
{
	trainImages = X;
	trainLabels = y;
}


Vector KNNClassifier::predict(Matrix X)
{
    auto ret = Vector(X.rows());
	
	#pragma omp parallel for
	for(unsigned i = 0; i < X.rows(); i++){
		priority_queue<pair<double,unsigned>> vecinos;
		auto resta = Vector(X.cols());
		double norma = 0;
		for(unsigned j = 0; j < trainImages.rows(); j++){
			for(unsigned k = 0; k < trainImages.cols(); k++){
				resta(k) = X(i,k) - trainImages(j,k);
			}
			norma = resta.norm();
			if(vecinos.size() < k){
				vecinos.push(make_pair(norma,trainLabels(j)));
			}else if(vecinos.top().first > norma){
				vecinos.pop();
				vecinos.push(make_pair(norma,trainLabels(j)));	
			}
		}	

		vector<unsigned> contador(10);
		while(vecinos.size() != 0){
			auto tmp = vecinos.top().second;
			vecinos.pop();
			contador[tmp]++;
		}

		double maximo = 0;
		for(unsigned i = 1; i < contador.size(); i++){
			if(contador[maximo] < contador[i]){
				maximo = i;
			}
		}

		ret(i) = maximo;
	}
    return ret;
}
