#include <algorithm>
#include <chrono>
#include <iostream>
#include "eigen.h"

using namespace std;


pair<double, Vector> power_iteration(const Matrix& X, unsigned num_iter, double eps)
{
    Vector b = Vector::Random(X.cols());
    Vector old = Vector::Zero(X.cols());
    for(unsigned i=0;i<num_iter && !(old - b).isZero(eps);i++){
        old=b;
        b = X * b;
        b.normalize();
    }
    double eigenvalue = b.transpose().dot(X * b) / b.norm();

    return make_pair(eigenvalue, b);
}

pair<Vector, Matrix> get_first_eigenvalues(const Matrix& X, unsigned num, unsigned num_iter, double epsilon)
{
    Matrix A(X);
    Vector eigvalues(num);
    Matrix eigvectors(A.rows(), num);

    for(unsigned i=0;i<num;i++){
        pair<double,Vector> eigens = power_iteration(A,num_iter,epsilon);
        eigvalues(i) = eigens.first;
        eigvectors.col(i) = eigens.second;
        A = A - eigens.first * eigens.second * eigens.second.transpose();

    }
    return make_pair(eigvalues, eigvectors);
}
