#include <iostream>
#include "pca.h"
#include "eigen.h"
#include "knn.h"
#include <pybind11/embed.h>

namespace py = pybind11;

PYBIND11_MODULE(knn,m){
	m.def("KNNClassifier", &KNNClassifier);
}
